
im_raw.module

This module allows you to enter ImageMagick command line options as an image effect.

imagemagick.module is required:
http://drupal.org/project/imagemagick

Take a look at http://www.imagemagick.org/Usage/ to get an idea of the sort of things you can do. But keep in mind that the Drupal image effect API puts constraints on what will work. Not everything you can do with ImageMagick will be possible in the context of an image effect.

There's a permission that will deny users the ability to access the form to edit the commands, so check the permission is enabled for any role you want to have access.

Ensure that no WYSIWYG editors are enabled on the im_raw command text aread when editing the image style.

Adrian Simmons (aka adrinux)
adrian@perlucida.com

